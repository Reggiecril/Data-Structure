package set;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		ArrayList<String> list=new ArrayList<>();
		FileOperation fo=new FileOperation();
		if(fo.readFile("pride-and-prejudice.txt", list)) {
			System.out.println("ArrayList: "+list.size());
			BSTSet<String> bst=new BSTSet<String>();
			for(String s:list) {
				bst.add(s);
			}
			System.out.println("BSTSet:"+bst.getSize());
			
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			LinkedListSet<String> lls=new LinkedListSet<>();
			for(String s:list) {
				lls.add(s);
			}
			System.out.println("LinkdedList:"+lls.getSize());
		}
		
	}

}
