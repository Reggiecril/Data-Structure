package listNode;

public class LinkedList<E> {
	private class ListNode {
		E val;
		ListNode next;

		ListNode() {
			val = null;
			next = null;
		}

		ListNode(E val) {
			this.val = (E)new Object();
		}

		ListNode(E val, ListNode next) {
			this.val = val;
			this.next = next;
		}
	}

	private int size;
	private ListNode dummyHead;

	public LinkedList() {
		dummyHead = new ListNode();
		size = 0;
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void add(int index, E e) {
		if (index < 0 || index > size)
			throw new IllegalArgumentException("Add failed. Illegal index.");
		ListNode pre = dummyHead;
		for (int i = 0; i < index; i++) {
			pre = pre.next;
		}
		pre.next = new ListNode(e, pre.next);
		size++;
	}
	public void addFirst(E e) {
		add(0,e);
	}
	public void addLast(E e) {
		add(size,e);
	}
	public E get(int index) {
		if (index < 0 || index > size)
			throw new IllegalArgumentException("Add failed. Illegal index.");
		ListNode pre = dummyHead;
		for (int i = 0; i < index; i++) {
			pre = pre.next;
		}
		return pre.val;
	}

	public void removeElement(E e) {
		ListNode pre=dummyHead;
		ListNode cur =pre.next;
		while(cur!=null) {
			if(cur.val.equals(e)) {
				pre.next=cur.next;
			}
			cur=cur.next;	
		}
	}
	public E remove(int index) {
		ListNode pre=dummyHead;
		ListNode cur =pre.next;
		E ex=(E)new Object();
		for(int i=0;i<index;i++) {
			cur=cur.next;
		}
		ex=cur.val;
		pre.next=cur.next;
		return ex;
	}
	public E removeFirst() {
		return remove(0);
	}
	public void set(int index, E e) {
		if (index < 0 || index > size)
			throw new IllegalArgumentException("Add failed. Illegal index.");
		ListNode pre = dummyHead;
		for (int i = 0; i < index; i++) {
			pre = pre.next;
		}
		pre.val = e;
	}

	public boolean contains(E e) {
		ListNode cur = dummyHead.next;
		while (cur != null) {
			if (cur.val.equals(e))
				return true;
			cur = cur.next;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (ListNode cur = dummyHead.next; cur != null; cur = cur.next)
			sb.append(cur.val);
		return sb.toString();
	}

	public static void main(String[] args) {
		LinkedList<Integer> ll = new LinkedList<Integer>();
		for (int i = 0; i < 10; i++) {
			ll.add(i, i + 1);
			System.out.println(ll);
		}
		ll.add(2, 3213);
		System.out.println(ll);
		System.out.println(ll.removeFirst());
		System.out.println(ll);
	}
}
