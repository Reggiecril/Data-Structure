package avltree;

import map.Map;

public class AVLMap<K extends Comparable<K>,V> implements Map<K,V> {

	private AVLTree avl;
	
	AVLMap(){
		
		avl=new AVLTree<>();
		
	}

	@Override
	public void add(K key, V value) {
		avl.add(key, value);		
	}

	@Override
	public V remove(K key) {
		return (V) avl.remove(key);
	}

	@Override
	public boolean contain(K key) {
		return avl.contains(key);
	}

	@Override
	public void set(K key, V value) {
		avl.set(key, value);
	}

	@Override
	public V get(K key) {
		return (V) avl.get(key);
	}

	@Override
	public int getSize() {
		return avl.getSize();
	}

	@Override
	public boolean isEmpty() {
		return avl.isEmpty();
	}
	

}
