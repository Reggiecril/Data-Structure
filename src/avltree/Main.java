package avltree;

import java.util.ArrayList;

import map.BSTMap;
import map.LinkedListMap;
import map.Map;
import set.Set;

public class Main {
	private static double testMap(Map<String, Integer> map, String filename) {

		long startTime = System.nanoTime();

		System.out.println(filename);
		ArrayList<String> words = new ArrayList<>();
		if (FileOperation.readFile(filename, words)) {
			System.out.println("Total words: " + words.size());

			for (String word : words) {
				if (map.contain(word))
					map.set(word, map.get(word) + 1);
				else
					map.add(word, 1);
			}

			System.out.println("Total different words: " + map.getSize());
			System.out.println("Frequency of PRIDE: " + map.get("pride"));
			System.out.println("Frequency of PREJUDICE: " + map.get("prejudice"));
		}

		long endTime = System.nanoTime();

		return (endTime - startTime) / 1000000000.0;
	}

	private static double testSet(Set<String> set, String filename) {

		long startTime = System.nanoTime();

		System.out.println(filename);
		ArrayList<String> words = new ArrayList<>();
		if (FileOperation.readFile(filename, words)) {
			System.out.println("Total words: " + words.size());

			for (String word : words)
				set.add(word);
			System.out.println("Total different words: " + set.getSize());
		}

		long endTime = System.nanoTime();

		return (endTime - startTime) / 1000000000.0;
	}

	public static void main(String[] args) {
		String filename = "pride-and-prejudice.txt";

		BSTMap<String, Integer> bstMap = new BSTMap<>();
		double time1 = testMap(bstMap, filename);
		System.out.println("BST Map: " + time1 + " s");

		System.out.println();

		LinkedListMap<String, Integer> linkedListMap = new LinkedListMap<>();
		double time2 = testMap(linkedListMap, filename);
		System.out.println("Linked List Map: " + time2 + " s");

		System.out.println();

		AVLMap<String, Integer> avlMap = new AVLMap<>();
		double time3 = testMap(avlMap, filename);
		System.out.println("AVL Map: " + time3 + " s");
		System.out.println();
		AVLSet<String> avlSet = new AVLSet<>();
		double time4 = testSet(avlSet, filename);
		System.out.println("AVL Set: " + time4 + " s");
	}

}
