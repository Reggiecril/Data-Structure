package tree;

import java.util.Stack;

public class BST<E extends Comparable<E>> {
	private class Node {
		public E e;
		public Node left, right;

		public Node(E e) {
			this.e = e;
			left = null;
			right = null;
		}

	}

	private Node root;
	private int size;

	public BST() {
		root = null;
		size = 0;
	}

	public int getSize() {
		return size;

	}

	public boolean isEmpty() {
		return size == 0;
	}

	// 增加节点到二分搜索树
	public void add(E e) {
		root = add(root, e);
	}

	private Node add(Node node, E e) {
		if (node == null) {
			size++;
			return new Node(e);
		}

		if (e.compareTo(node.e) > 0)
			node.right = add(node.right, e);
		else if(e.compareTo(node.e) < 0)
			node.left = add(node.left, e);
		return node;
	}

	public boolean contains(E e) {
		return contains(root, e);
	}

	private boolean contains(Node node, E e) {
		if (node == null)
			return false;
		if (e.compareTo(node.e) == 0)
			return true;
		else if (e.compareTo(node.e) < 0)
			return contains(node.left, e);
		else
			return contains(node.right, e);
	}

	// 前序遍历
	public void preOrder() {
		preOrder(root);
	}

	private void preOrder(Node node) {
		if (node != null) {
			System.out.println(node.e);
			preOrder(node.left);
			preOrder(node.right);
		}
	}

	// 前序遍历 非递归
	public void preOrderNR() {
		preOrderNR(root);
	}

	private void preOrderNR(Node node) {
		Stack<Node> stack = new Stack<Node>();
		stack.push(node);
		while (!stack.isEmpty()) {
			Node next = stack.pop();

			if (next.right != null)
				stack.push(next.right);
			if (next.left != null)
				stack.push(next.left);
			System.out.println(next.e);
		}
	}

	// 中序遍历
	public void inOrder() {
		inOrder(root);
	}

	private void inOrder(Node node) {
		if (node != null) {

			inOrder(node.left);
			System.out.println(node.e);
			inOrder(node.right);
		}
	}

	// 后序遍历
	public void postOrder() {
		postOrder(root);
	}

	private void postOrder(Node node) {
		if (node != null) {
			postOrder(node.left);
			postOrder(node.right);
			System.out.println(node.e);
		}
	}

	public E minimum() {
		return minimum(root).e;
	}

	private Node minimum(Node node) {
		if (node.left == null)
			return node;
		node.left = minimum(node.left);
		return node.left;
	}

	public E removeMinimum() {

		removeMinimum(root);
		return minimum();
	}

	private Node removeMinimum(Node node) {
		if (node.left == null) {
			Node rightNode = node.right;
			node.right = null;
			size--;
			return rightNode;
		}
		node.left = removeMinimum(node.left);
		return node;
	}

	public E maximum() {
		return maximum(root).e;
	}

	private Node maximum(Node node) {
		if (node.right == null)
			return node;
		node.right = maximum(node.right);
		return node.right;
	}

	public E removeMaximum() {

		removeMaximum(root);
		return maximum();
	}

	private Node removeMaximum(Node node) {
		if (node.right == null) {
			Node leftNode = node.left;
			node.left = null;
			size--;
			return leftNode;
		}
		node.right = removeMaximum(node.right);
		return node;
	}

	public void remove(E e) {
		root = remove(root, e);
	}

	private Node remove(Node node, E e) {
		if(node==null)
			return null;
		if(e.compareTo(node.e)>0) {
			node.right=remove(node,e);
			return node;
		}else if(e.compareTo(node.e)<0) {
			node.left=remove(node,e);
			return node;
		}else {
			if(node.right==null) {
				Node leftNode=node.left;
				node.left=null;
				size--;
				return leftNode;
				
			}
			if(node.left==null) {
				Node rightNode=node.right;
				node.right=null;
				size--;
				return rightNode;
			}
			Node successor=minimum(node.right);
			successor.right=removeMinimum(node.right);
			successor.left=node.left;
			
			node.left=node.right=null;
		return successor;	
		}
		
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();

		generateBSTString(root, 0, res);
		return res.toString();
	}

	private void generateBSTString(Node node, int depth, StringBuilder res) {
		if (node == null) {
			res.append(generateDepthString(depth) + "null\n");
			return;
		}
		res.append(generateDepthString(depth) + node.e + "\n");
		generateBSTString(node.left, depth + 1, res);
		generateBSTString(node.right, depth + 1, res);

	}

	private String generateDepthString(int depth) {
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			res.append("--");
		}
		return res.toString();
	}
}
