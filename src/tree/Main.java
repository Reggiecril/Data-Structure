package tree;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		BST<Integer> bst = new BST<Integer>();
		int[] num = new int[] { 32, 2, 3, 5, 21, 67, 23, 54, 12, 534 };
		for (int i : num)
			bst.add(i);
		bst.preOrderNR();
		System.out.println();
		bst.preOrder();
		System.out.println();
		bst.inOrder();
		System.out.println();

		bst.postOrder();
		System.out.println();

		System.out.println("------------");
		System.out.println(bst.toString());
		System.out.println("minimum:  "+bst.minimum());
		System.out.println("maximum:  "+bst.maximum());
		
		Random random=new Random();
		for(int i=0;i<1000;i++) 
			bst.add(random.nextInt(10000));
		
		for(int i=0;i<1000;i++)
			System.out.println(bst.removeMaximum());
	}

}
