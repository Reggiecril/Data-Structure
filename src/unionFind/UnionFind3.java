package unionFind;
//size优化
public class UnionFind3 implements UF {
	private int[] parent;
	private int[] sz;
	UnionFind3() {

	}

	UnionFind3(int size) {
		parent = new int[size];
		sz=new int[size];
		for (int i = 0; i < size; i++) {
			parent[i] = i;
			//一个单位占1
			sz[i]=1;
		}
			
	}

	@Override
	public int getSize() {
		return parent.length;
	}

	private int find(int element) {
		if(element<0&&element>=parent.length)
			throw new IllegalArgumentException("out of index ");
		while(element!=parent[element])
			element=parent[element];
		return element;
	}

	@Override
	public boolean isConnect(int q, int p) {
		return find(q) == find(p);
	}

	// 合并所属元素p和元素q的集合
	@Override
	public void unionElements(int q, int p) {
		int qRoot=find(q);
		int pRoot=find(p);
		if(qRoot==pRoot)
			return;
		if(sz[qRoot]>sz[pRoot]) {
			parent[pRoot]=qRoot;
			sz[qRoot]+=sz[pRoot];
		}else {
			parent[qRoot]=pRoot;
			sz[pRoot]+=sz[qRoot];
		}
		

	}

}
