package unionFind;

public class UnionFind1 implements UF {
	private int[] id;

	UnionFind1() {

	}

	UnionFind1(int size) {
		id = new int[size];
		for (int i = 0; i < size; i++)
			id[i] = i;
	}

	@Override
	public int getSize() {
		return id.length;
	}

	private int find(int element) {
		if(element<0&&element>=id.length)
			throw new IllegalArgumentException("out of index ");
		return id[element];
	}

	@Override
	public boolean isConnect(int q, int p) {
		return find(q) == find(p);
	}

	// 合并所属元素p和元素q的集合
	@Override
	public void unionElements(int q, int p) {
		int qID = find(q);
		int pID = find(p);
		for (int i = 0; i < id.length; i++) {
			if (i == pID)
				id[i] = qID;
		}

	}

}
