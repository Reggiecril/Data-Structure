package unionFind;
//路径压缩
public class UnionFind6 implements UF {
	private int[] parent;
	private int[] rank;
	UnionFind6() {

	}

	UnionFind6(int size) {
		parent = new int[size];
		rank = new int[size];
		for (int i = 0; i < size; i++) {
			parent[i] = i;
			//一个单位占1
			rank[i]=1;
		}
			
	}

	@Override
	public int getSize() {
		return parent.length;
	}

	private int find(int element) {
		if(element<0&&element>=parent.length)
			throw new IllegalArgumentException("out of index ");
		if(element!=parent[element])
		{
			parent[element]=find(parent[element]);
		}
		return parent[element];
	}

	@Override
	public boolean isConnect(int q, int p) {
		return find(q) == find(p);
	}

	// 合并所属元素p和元素q的集合
	@Override
	public void unionElements(int q, int p) {
		int qRoot=find(q);
		int pRoot=find(p);
		if(qRoot==pRoot)
			return;
		if(rank[qRoot]>rank[pRoot]) {
			parent[pRoot]=qRoot;
		}else if(rank[qRoot]<rank[pRoot]){
			parent[qRoot]=pRoot;
		}else {
			parent[qRoot]=pRoot;
			rank[pRoot]+=1;
		}
		

	}

}
