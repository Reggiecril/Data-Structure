package unionFind;

public interface UF {
	int getSize();
	boolean isConnect(int q,int p);
	void unionElements(int q,int p);
}
