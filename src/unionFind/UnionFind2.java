package unionFind;

public class UnionFind2 implements UF {
	private int[] parent;

	UnionFind2() {

	}

	UnionFind2(int size) {
		parent = new int[size];
		for (int i = 0; i < size; i++)
			parent[i] = i;
	}

	@Override
	public int getSize() {
		return parent.length;
	}

	private int find(int element) {
		if(element<0&&element>=parent.length)
			throw new IllegalArgumentException("out of index ");
		while(element!=parent[element])
			element=parent[element];
		return element;
	}

	@Override
	public boolean isConnect(int q, int p) {
		return find(q) == find(p);
	}

	// 合并所属元素p和元素q的集合
	@Override
	public void unionElements(int q, int p) {
		int qRoot=find(q);
		int pRoot=find(p);
		if(qRoot==pRoot)
			return;
		parent[qRoot]=pRoot;

	}

}
