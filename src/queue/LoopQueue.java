package queue;

import array.Array;

public class LoopQueue<E> implements Queue<E>{

	E[] data;
	int front,tail;
	int size;
	
	public LoopQueue() {
		this(10);
	}
	public LoopQueue(int capacity) {
		data=(E[]) new Object[capacity];
	}
	@Override
	public void enqueue(E e) {
		if((tail+1)%data.length==front)
			resize(getCapacity()*2);
		data[tail]=e;
		size++;
	}

	@Override
	public E dequeue() {
		E rem=data[front];
		front=(front+1)%data.length;
		size--;
		if(size==getCapacity()/4&&getCapacity()/2!=0) 
			resize(getCapacity()/2);
		return rem;	
			
		
	}
	private void resize(int newCapacity) {
		E[] newData=(E[])new Object[newCapacity+1];
		for(int i=0;i<size;i++) 
			newData[i]=data[(front+i)%data.length];
		data=newData;
		front=0;
		tail=size;
	}
	@Override
	public E getFront() {
		return data[front];
	}

	@Override
	public int getSize() {
		return size;
	}
	public int getCapacity() {
		return size+1;
	}
	@Override
	public boolean isEmpty() {
		return front==tail;
	}
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("Queue:");
		sb.append("front [");
		for(int i=0;i<size;i++) {
			sb.append(data[i]);
			if(i!=getSize()-1)
				sb.append(",");
		}
		sb.append("] tail");
		return sb.toString();
	}
}
