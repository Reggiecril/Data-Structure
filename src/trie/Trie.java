package trie;

import java.util.TreeMap;

public class Trie {
	private class Node{
		public boolean isWord;
		public TreeMap<Character,Node> next;
		Node(){
			this(false);
		}
		Node(boolean isWord){
			this.isWord=isWord;
			next=new TreeMap<>();
		}
	}
	private Node root;
	private int size;
	
	Trie(){
		root=new Node();
		size=0;
	}
	public int getSize() {
		return size;
	}
	public boolean isEmpty() {
		return size==0;
	}
	public void add (String word) {
		Node cur=root;
		for(int i=0;i<word.length();i++) {
			if(cur.next.get(word.charAt(i))==null)
				cur.next.put(word.charAt(i), new Node());
			cur=cur.next.get(word.charAt(i));
		}
		if(!cur.isWord)
			cur.isWord=true;
		size++;
	}
	
	public boolean contains(String word) {
		Node cur=root;
		for(int i=0;i<word.length();i++) {
			if(cur.next.get(word.charAt(i))==null)
				return false;
			cur=cur.next.get(word.charAt(i));
		}
		return cur.isWord;
	}
	public boolean isPrefix(String prefix) {
		Node cur=root;
		for(int i=0;i<prefix.length();i++) {
			if(cur.next.get(prefix.charAt(i))==null)
				return false;
			cur=cur.next.get(prefix.charAt(i));
		}
		return true;
	}
}
