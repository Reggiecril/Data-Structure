package map;


public class BSTMap<K extends Comparable<K>,V> implements Map<K,V> {
	private class Node{
		public K key;
		public V value;
		public Node left;
		public Node right;
		public Node() {
			this.key=null;
			this.value=null;
			this.left=null;
			this.right=null;
		}
		public Node(K key,V value) {
			this.key=key;
			this.value=value;
			this.left=null;
			this.right=null;
		}
	}
	private Node root;
	private int size;
	public BSTMap(){
		root=null;
		size=0;
	}
	//add
	@Override
	public void add(K key, V value) {
		root=add(root,key,value);
	}
	private Node add(Node root,K key,V value) {
		if(root==null) {
			size++;
			return new Node(key,value);
		}
		if(key.compareTo(root.key)>0) {
			root.right=add(root.right,key,value);
		}else if(key.compareTo(root.key)<0) {
			root.left=add(root.left,key,value);
		}else {
			root.value=value;
		}
		return root;
	}
	//remove
	@Override
	public V remove(K key) {
		Node node=getNode(root,key);
		root=remove(root,key);
		return node.value;
	}
	private Node remove(Node root,K key) {
		if(root==null) {
			return null;
		}
		if(key.compareTo(root.key)<0) {
			root.left=remove(root.left,key);
			return root;
		}else if(key.compareTo(root.key)>0) {
			root.right=remove(root.right,key);
			return root;
		}else {
			if(root.right==null) {
				Node leftNode=root.left;
				root.left=null;
				size--;
				return leftNode;
			}
			if(root.left==null) {
				Node rightNode=root.right;
				root.right=null;
				size--;
				return rightNode;
			}
			Node successor=maximum(root.left);
			root.left=removeMaximum(root.left);
			successor.right=root.right;
			return successor;
		}
	}
	private Node maximum(Node node) {
		if (node.right == null)
			return node;
		node.right = maximum(node.right);
		return node.right;
	}

	private Node removeMaximum(Node node) {
		if (node.right == null) {
			Node leftNode = node.left;
			node.left = null;
			size--;
			return leftNode;
		}
		node.right = removeMaximum(node.right);
		return node;
	}
	private Node getNode(Node root,K key) {
		if(root==null)
			return null;
		if(key.compareTo(root.key)>0){
			return getNode(root.right,key);
		}else if(key.compareTo(root.key)<0) {
			return getNode(root.left,key);
		}else {
			return root;
		}
	}
	//contain
	@Override
	public boolean contain(K key) {
		return getNode(root,key)!=null;
	}
	
	@Override
	public void set(K key, V value) {
		if(getNode(root,key)==null) {
			throw new IllegalArgumentException("");
		}else {
			getNode(root,key).value=value;
		}
			
	}
	@Override
	public V get(K key) {
		return getNode(root,key)==null?null:getNode(root,key).value;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0;
	}
	
}
