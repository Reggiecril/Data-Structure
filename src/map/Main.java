package map;

import java.util.ArrayList;

public class Main {
	public static void main(String []args) {
		ArrayList<String> list=new ArrayList<>();
		FileOperation fo=new FileOperation();
		if(fo.readFile("pride-and-prejudice.txt", list)) {
			System.out.println(list.size());
//			LinkedListMap<String,Integer> map=new LinkedListMap<>();
//			for(String word:list) {
//				if(map.contain(word))
//					map.set(word, map.get(word)+1);
//				else
//					map.add(word, 1);
//			}
//			System.out.println("Map:"+map.getSize());
//			System.out.println(map.get("pride"));
			
			System.out.println("---------------------------");
			BSTMap<String,Integer> bstMap=new BSTMap<>();
			for(String word:list) {
				if(bstMap.contain(word))
					bstMap.set(word, bstMap.get(word)+1);
				else
					bstMap.add(word, 1);
			}
			System.out.println("bstMap:"+bstMap.getSize());
			System.out.println(bstMap.get("pride"));
		}
	}
}
