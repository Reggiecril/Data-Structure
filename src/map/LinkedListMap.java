package map;

public class LinkedListMap<K,V> implements Map<K,V> {
	private class Node{
		public K key;
		public V value;
		public Node next;
		
		public Node(){
			key=null;
			value=null;
			next=null;
		}
		public Node(K key,Node next){
			this.key=key;
			this.next=next;
			this.value=null;
		}
		public Node(K key,V value,Node next){
			this.key=key;
			this.next=next;
			this.value=value;
		}
		@Override 
		public String toString() {
			return key.toString()+" : "+value.toString();
		}
	}
	private Node dummyHead;
	private int size;
	public LinkedListMap() {
		dummyHead=new Node();
		size=0;
	}
	private Node getNode(K key) {
		Node pre=dummyHead.next;
		while(pre!=null) {
			if(pre.key.equals(key))
				return pre;
			pre=pre.next;
		}
		return null;
	}
	@Override
	public void add(K key, V value) {
		Node node=getNode(key);
		if(node==null) {
			dummyHead.next=new Node(key,value,dummyHead.next);
			size++;
		}else {
			node.value=value;
		}
	}

	@Override
	public V remove(K key) {
		Node pre=dummyHead;
		while(pre.next!=null) {
			if(pre.next.key.equals(key))
				break;
			pre=pre.next;
		}
		if(pre.next!=null) {
			Node delNode=pre.next;
			pre.next=delNode.next;
			delNode.next=null;
			size--;
			return delNode.value;
		}
		return null;
	}

	@Override
	public boolean contain(K key) {
		return getNode(key)!=null;
	}

	@Override
	public void set(K key, V value) {
		Node node=getNode(key);
		if(node==null) {
			throw new IllegalArgumentException("");
			
		}else {
			node.value=value;
		}
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}
	@Override
	public V get(K key) {
		return getNode(key)==null?null:getNode(key).value;
		
	}
	
}
