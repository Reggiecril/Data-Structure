package segmentTree;

public interface Merge<E> {
	E merger(E a, E b);
}
