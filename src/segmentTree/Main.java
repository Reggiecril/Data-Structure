package segmentTree;

public class Main {
	public static void main(String []args) {
		Integer []num= {1,2,3,4,5,6};
		SegmentTree<Integer> tree=new SegmentTree<Integer>(num,(a,b)->a+b);
		System.out.println(tree.query(0, 4));
		
		tree.set(3, 0);
		System.out.println(tree.query(0, 4));

	}
}
