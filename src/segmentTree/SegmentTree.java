package segmentTree;

import java.util.Arrays;

public class SegmentTree<E> {
	private E[] tree;
	private E[] data;
	private Merge<E> merge;
	public SegmentTree(E[] arr,Merge<E> merge) {
		this.merge=merge;
		data=(E[])new Object[arr.length];
		for(int i=0;i<arr.length;i++)
			data[i]=arr[i];
		
		tree=(E[])new Object[arr.length*4];
		buildSegmentTree(0,0,data.length-1);
	}
	private void buildSegmentTree(int treeIndex,int l,int r) {
		if(l==r) {
			tree[treeIndex]=data[l];
			return;
		}
		int leftTreeIndex=leftChild(treeIndex);
		int rightTreeIndex=rightChild(treeIndex);
		int mid=l+(r-l)/2;
		
		buildSegmentTree(leftTreeIndex,l,mid);
		buildSegmentTree(rightTreeIndex,mid+1,r);
		
		tree[treeIndex]=merge.merger(tree[leftTreeIndex], tree[rightTreeIndex]);

	}
	
	public E query(int queryL,int queryR) {
		if(queryL<0||queryL>=data.length||queryR<0||queryR>=data.length)
			throw new IllegalArgumentException("It's illagel");
		return query(0,0,data.length-1,queryL,queryR);
	}
	
	private E query(int treeIndex,int l,int r,int queryL,int queryR) {
		if(l==queryL&&r==queryR) {
			return tree[treeIndex];
		}
		int leftTreeIndex=leftChild(treeIndex);
		int rightTreeIndex=rightChild(treeIndex);
		int mid=l+(r-l)/2;
		
		if(queryL>=mid+1) {
			return query(rightTreeIndex,mid+1,r,queryL,queryR);
		}else if(queryR<=mid) {
			return query(leftTreeIndex,l,mid,queryL,queryR);
		}
		
		E leftResult=query(leftTreeIndex,l,mid,queryL,mid);
		E rightResult=query(rightTreeIndex,mid+1,r,mid+1,queryR);
		return merge.merger(leftResult, rightResult);
	}
	
	public void set(int index,E e) {
		if(index<0||index>=data.length)
			throw new IllegalArgumentException("Index is illegal");
		set(0,0,data.length-1,index,e);
	}
	private void set(int treeIndex,int l,int r,int index,E e) {
		if(l==r) {
			tree[treeIndex]=e;
			return;
		}
		int leftTreeIndex=leftChild(treeIndex);
		int rightTreeIndex=rightChild(treeIndex);
		int mid=l+(r-l)/2;
		
		if(index<=mid)
			set(leftTreeIndex,l,mid,index,e);
		else
			set(rightTreeIndex,mid+1,r,index,e);
		tree[treeIndex]=merge.merger(tree[leftTreeIndex], tree[rightTreeIndex]);
	}
	public int getSize() {
		return data.length;
	}
	public E get(int index) {
		if(index<0||index>=data.length)
			throw new IllegalArgumentException("Index is illegal");
		return data[index];
	}
	
	public int leftChild(int index) {
		return 2*index+1;
	}
	public int rightChild(int index) {
		return 2*index+2;
	}
	@Override
	public String toString() {
		return "SegmentTree [tree=" + Arrays.toString(tree) + "]";
	}
	
}
