package stack;

public interface Stack<E> {
	int getSize();
	E pop();
	E peek();
	boolean isEmpty();
	void push(E e);
}
