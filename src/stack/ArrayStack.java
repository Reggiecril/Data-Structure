package stack;

import array.Array;

public class ArrayStack<E> implements Stack<E>{
	Array<E> array;
	public ArrayStack() {
		array=new Array<E>();
	}
	public ArrayStack(int capacity) {
		array=new Array<E>(capacity);
	}
 	@Override
	public int getSize() {
		return array.getSize();
	}

	@Override
	public E pop() {
		return array.removeLast();
	}

	@Override
	public E peek() {
		return array.getLast();
	}

	@Override
	public boolean isEmpty() {
		return array.isEmpty();
	}

	@Override
	public void push(E e) {
		array.addFirst(e);
	}

	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("Stack:");
		sb.append('[');
		for(int i=0;i<array.getSize();i++) {
			sb.append(array.get(i));
			if(i!=getSize()-1)
				sb.append(",");
		}
		sb.append("] top");
		return sb.toString();
	}

}
