package stack;

import listNode.LinkedList;

public class LinkedListStack<E> implements Stack<E> {
	private LinkedList<E> ll;
	LinkedListStack(){
		ll=new LinkedList<E>();
	}
	@Override
	public int getSize() {
		return ll.getSize();
	}

	@Override
	public E pop() {
		return ll.removeFirst();
	}

	@Override
	public E peek() {
		return ll.get(0);
	}

	@Override
	public boolean isEmpty() {
		return ll.isEmpty();
	}

	@Override
	public void push(E e) {
		ll.addFirst(e);
		
	}
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("Stack:");
		sb.append('[');
		sb.append(ll);
		sb.append("] top");
		return sb.toString();
	}
}
