package heap;

import java.util.Random;

public class Main {
	
	public static void main(String[] args) {
		MaxHeap<Integer> heap=new MaxHeap<>();
		Random random=new Random();
		int[] num=new int[10000];
		for(int i=0;i<10000;i++) {
			heap.add(random.nextInt(1000));
		}
		for(int i=0;i<10000;i++) {
			num[i]=heap.extractMax();
		}
		for(int i=1;i<10000;i++) {
			if(num[i]>num[i-1])
				throw new IllegalArgumentException("out of index");
		}	
	}
}
