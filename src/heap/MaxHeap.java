package heap;

import array.Array;

public class MaxHeap<E extends Comparable<E>> {
	private Array<E> data;

	public MaxHeap() {
		this(10);
	}

	public MaxHeap(int capacity) {
		data = new Array<>(capacity);
	}

	public MaxHeap(E[] arr) {
		data = new Array<>(arr);
		for(int i=arr.length-1;i>=0;i--)
			siftDown(i);
	}
	
	public int getSize() {
		return data.getSize();
	}

	public boolean isEmpty() {
		return data.isEmpty();
	}

	private int parent(int index) {
		if (index == 0)
			throw new IllegalArgumentException("index-0 doesn't hava parent");
		return (index - 1) / 2;
	}

	private int leftChild(int index) {
		return index * 2 + 1;
	}

	private int rightChild(int index) {
		return index * 2 + 2;
	}

	public void add(E e) {
		data.addLast(e);
		siftUp(getSize()-1);
	}

	private void siftUp(int k) {
		while (k > 0 && data.get(parent(k)).compareTo(data.get(k))<0) {
			if(k<0||k>=data.getSize()||parent(k)<0||parent(k)>=data.getSize()){
				throw new IllegalArgumentException("OutOfIndex");
			
			}
			E e=data.get(k);
			data.set(k, data.get(parent(k)));
			data.set(parent(k), e);
			
			k=parent(k);
		}
	}
	// 看堆中的最大元素
    public E findMax(){
        if(data.getSize() == 0)
            throw new IllegalArgumentException("Can not findMax when heap is empty.");
        return data.get(0);
    }
	public E extractMax() {
		E e=findMax();
		E swap=data.get(0);
		data.set(0, data.get(data.getSize()-1));
		data.set(data.getSize()-1,swap);
		data.removeLast();
		siftDown(0);
		
		return e;
	}
	//取出最大值，替换
	public E replace(E e) {
		E ret=findMax();
		data.set(0, e);
		siftDown(0);
		return findMax();
	}
	private void siftDown(int k) {
		while (leftChild(k)<data.getSize()) {
			
			int j=leftChild(k);
			//get child maximum leaf
			if(j+1<data.getSize()&&data.get(j+1).compareTo(data.get(j))>0)
				j=rightChild(k);
			if(data.get(k).compareTo(data.get(j))>0)
				break;
			
			E e=data.get(k);
			data.set(k, data.get(j));
			data.set(j, e);
			
			k=j;
		}
	}
}
