package array;

public class Array<E> {
	private E[] data;
	private int size;

	// 默认数组长度
	public Array() {
		this(10);
	}

	// 初始化数组长度
	@SuppressWarnings("unchecked")
	public Array(int capacity) {
		data = (E[]) new Object[capacity];
		size = 0;
	}
	
	public Array(E[] arr) {
		data=(E[]) new Object[arr.length];
		size=arr.length;
		for(int i=0;i<arr.length;i++)
			data[i]=arr[i];
	}

	// 返回数组大小
	public int getSize() {
		return size;
	}

	// 返回数组容量
	public int getCapacity() {
		return data.length;
	}

	// 是否为空
	public boolean isEmpty() {
		return size == 0;
	}



	// 获得指定位置的值
	public E get(int index) {
		if (index < 0 || index > size)
			throw new IllegalArgumentException("get is failed. It is out of array index");
		return data[index];
	}
	public E getFirst() {
		return get(0);
	}
	public E getLast() {
		return get(size-1);
	}
	// 更改指定位置数值
	public void set(int index, E num) {
		if (index < 0 || index > size)
			throw new IllegalArgumentException("get is failed. It is out of array index");
		data[index] = num;
	}

	// 判断是否有数值num
	public boolean contain(E num) {
		boolean flag = false;
		for (int i = 0; i < size; i++) {
			if (data[i].equals(num))
				flag = true;
		}
		return flag;

	}

	// 查找num在数组中的索引,如没有返回-1
	public int find(E num) {
		int count = -1;
		for (int i = 0; i < size; i++) {
			if (data[i].equals(num)) {
				count = i;
				break;
			}
		}
		return count;
	}

	// 加入元素到第一个
	public void addFirst(E num) {
		add(0, num);
	}
	// 加入元素到数组末尾
	public void addLast(E num) {
		// if(size==data.length)
		// throw new IllegalArgumentException("AddLast is failed. The array is full");
		// data[size]=num;
		// size++;
		add(size, num);
	}
	// 插入元素到数组
	public void add(int index, E num) {
		if (size == data.length)
			resize(data.length*2);
		if (index < 0 || index > size)
			throw new IllegalArgumentException("Add is failed. It is out of array index");

		for (int i = size - 1; i >= index; i--) {
			data[i + 1] = data[i];
		}
		data[index] = num;
		size++;
	}

	// 从数组中删除元素并返回
	public E remove(int index) {
		
		if (index < 0 || index > size)
			throw new IllegalArgumentException("Add is failed. It is out of array index");
		E count = data[index];
		for (int i = index; i < size - 1; i++) {

			data[i] = data[i + 1];
		}
		size--;
		data[size]=null;
		if(size==(data.length/4)&&data.length>1)
			resize(data.length/2);
		return count;
	}

	// 删除第一个元素
	public E removeFirst() {
		return remove(0);
	}

	// 删除最后一个元素
	public E removeLast() {
		return remove(size - 1);
	}

	// 删除指定的元素
	public void removeElement(E num) {
		if (find(num) != -1)
			remove(find(num));
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[Size:" + size + "]");
		sb.append("[Data:");
		for (int i = 0; i < size; i++) {
			sb.append(data[i] + "  ");
		}
		sb.append("]");

		return sb.toString();

	}
	
	private void resize(int newCapacity) {
		E[] newData=(E[])new Object[newCapacity];
		for(int i=0;i<size;i++)
			newData[i]=data[i];
		data=newData;
	}
}
